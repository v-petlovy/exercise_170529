class CreateGroupEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :group_events do |t|
      t.string :name, default: ''
      t.text :description, default: ''
      t.integer :duration, default: 30
      t.string :location, default: ''
      t.date :start_at
      t.date :end_at
      t.boolean :published, default: false
      t.boolean :deleted, default: false
      t.timestamps
    end
  end
end
