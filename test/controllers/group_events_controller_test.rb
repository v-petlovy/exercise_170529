require 'test_helper'

class GroupEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @group_event = group_events(:one)
  end

  test "should get index" do
    get group_events_url, as: :json
    assert_response :success
  end

  test "should create group_event" do
    assert_difference('GroupEvent.count') do
      post group_events_url, params: {group_event: {
          deleted: @group_event.deleted,
          description: @group_event.description,
          duration: @group_event.duration,
          end_at: @group_event.end_at,
          location: @group_event.location,
          name: @group_event.name,
          published: @group_event.published,
          start_at: @group_event.start_at}}, as: :json
    end
    assert_response 201
  end

  test "should show group_event" do
    get group_event_url(@group_event), as: :json
    group_event = JSON.parse(@response.body)

    assert_equal @group_event.name, group_event['name']
    assert_response :success
  end

  test "should update group_event" do
    patch group_event_url(@group_event), params: {group_event: {
        deleted: @group_event.deleted,
        description: @group_event.description,
        duration: @group_event.duration,
        end_at: @group_event.end_at,
        location: @group_event.location,
        name: @group_event.name,
        published: @group_event.published,
        start_at: @group_event.start_at}}, as: :json
    assert_response 200
  end

  test "should destroy group_event" do
    assert_difference('GroupEvent.deleted.count', 1) do
      delete group_event_url(@group_event), as: :json
    end
    assert_response :success
  end
end
