require 'test_helper'

class GroupEventTest < ActiveSupport::TestCase
  test 'should work with empty constructor' do
    group_event = GroupEvent.new()
    assert group_event.save
  end

  test 'should create not published with subset of attributes' do
    group_event = GroupEvent.new(
        name: 'hello'
    )
    assert group_event.save
  end

  test 'should not create published with subset of attributes' do
    group_event = GroupEvent.new(
        name: 'hello',
        published: true
    )
    assert_not group_event.save
  end

  test 'should set end date if duration is present' do
    group_event = GroupEvent.new(
        name: 'hello',
        start_at: 'Mon, 29 May 2017',
        duration: 30
    )
    group_event.duration = 30
    assert_equal group_event.end_at, 'Wed, 28 Jun 2017'.to_date
  end

  test 'should set duration if end_at is present' do
    group_event = GroupEvent.new(
        name: 'hello',
        start_at: 'Mon, 29 May 2017',
        end_at: 'Wed, 28 Jun 2017'
    )
    assert_equal group_event.duration, 30
  end

  test 'should not destroy object after deletion' do
    group_event = GroupEvent.create(
        name: 'hello',
        description: 'hello description',
        location: '@ office',
        published: true,
        start_at: 'Mon, 29 May 2017',
        end_at: 'Wed, 28 Jun 2017'
    )
    group_event.destroy

    assert_instance_of GroupEvent, GroupEvent.deleted.find(group_event.id)
  end
end
