class GroupEvent < ApplicationRecord
  validates :name, presence: true, if: :validate_event?
  validates :description, presence: true, if: :validate_event?
  validates :location, presence: true, if: :validate_event?
  validates :start_at, presence: true, if: :validate_event?
  validates :end_at, presence: true, if: :validate_event?
  validates :duration, presence: true, if: :validate_event?
  validates :duration, inclusion: {in: [30, 60]}

  scope :drafts, -> {where(published: false)}
  scope :published, -> {where(published: true)}
  scope :deleted, -> {unscoped.where(deleted: true)}

  default_scope {where(deleted: false)}

  def start_at=(date)
    set_duration(date, self.end_at) if (self.end_at && self.duration.blank?)
    set_end_at(date, self.duration) if (self.duration && self.end_at.blank?)
    super(date)
  end

  def duration=(days)
    set_start_at(self.end_at, days) if (self.end_at && self.start_at.blank?)
    set_end_at(self.start_at, days) if (self.start_at && self.end_at.blank?)
    super(days)
  end

  def end_at=(date)
    set_start_at(date, self.duration) if (self.duration && self.start_at.blank?)
    set_duration(self.start_at, date) if (self.start_at && self.duration.blank?)
    super(date)
  end

  def destroy
    self.update_column(:deleted, true)
  end

  private

  def validate_event?
    self.published_changed? && self.published_was == false
  end

  def set_start_at(end_at, duration)
    self.method(:start_at=).super_method.call(end_at.to_date - duration.to_i.days)
  end

  def set_duration(start_at, end_at)
    self.method(:duration=).super_method.call((end_at.to_date - start_at.to_date).to_i)
  end

  def set_end_at(start_at, duration)
    self.method(:end_at=).super_method.call(start_at.to_date + duration.to_i.days)
  end
end
